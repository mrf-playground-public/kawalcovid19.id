import React from 'react';
import { Box, Text, Paragraph, themeProps, BoxProps } from 'components/design-system';
import { ChevronIcon } from 'components/icons';
import styled from '@emotion/styled';
import useDarkMode from 'utils/useDarkMode';

interface CollapsibleCardProps extends BoxProps {
  title: React.ReactNode | string;
  icon?: React.ReactNode;
}

const CollapsibleCard: React.FC<CollapsibleCardProps> = ({ children, title, icon, ...rest }) => {
  const [isDarkMode] = useDarkMode();
  const [isOpen, setIsOpen] = React.useState(false);

  /** The base box that holds HeadBox and BodyBox. */
  const BaseBox = styled(Box)`
    display: flex;
    flex-direction: column;
  `;

  /** The upper-side box that retains. On click toggles the BodyBox display state below. */
  const HeadBox = styled(Box)`
    display: flex;
    align-content: center;
    justify-content: space-between;
  `;

  /** The collapsible lower-side box that toggles on HeadBox click. */
  const BodyBox = styled(Box)`
    flex-direction: column;
    align-items: stretch;
    border-top: 1px solid ${isDarkMode ? themeProps.colors.accents04 : themeProps.colors.accents07};
  `;

  return (
    <BaseBox p="md" bg="card" borderRadius={8} {...rest}>
      <HeadBox onClick={() => setIsOpen(!isOpen)} {...(isOpen && { pb: 'sm' })}>
        <Box mr="sm">
          {typeof title === 'string' ? (
            <Text variant={600} as="h3">
              {title}
            </Text>
          ) : (
            title
          )}
        </Box>
        <Box display="flex" alignItems="center">
          {icon || (
            <ChevronIcon
              width={16}
              height={16}
              fill={isDarkMode ? themeProps.colors.accents04 : themeProps.colors.accents07}
              style={{ transform: `rotate(${isOpen ? '-' : '+'}90deg)` }}
            />
          )}
        </Box>
      </HeadBox>
      <BodyBox display={isOpen ? 'flex' : 'none'} pt="xs">
        {children && typeof children === 'string' ? <Paragraph>{children}</Paragraph> : children}
      </BodyBox>
    </BaseBox>
  );
};

export default CollapsibleCard;
