import * as React from 'react';
import { NextPage } from 'next';
import { PageWrapper, Content, Column } from 'components/layout';
import { SectionHeader } from 'modules/posts-index';
import { Box, Heading, themeProps } from 'components/design-system';
import styled from '@emotion/styled';

import pemdaLinks from 'content/pemdaLinks.json';
import { PemerintahDaerahCard, PemerintahDaerahCardProps } from 'modules/pemerintah-daerah';

const Section = Content.withComponent('section');

interface PemerintahDaerahContentPageProps {
  pemdaList?: PemerintahDaerahCardProps;
  errors?: string;
}

const GridWrapper = styled(Box)`
  display: grid;
  grid-template-columns: repeat(auto-fill, 1fr);
  grid-column-gap: ${themeProps.space.md}px;

  ${themeProps.mediaQueries.md} {
    grid-template-columns: repeat(
      auto-fill,
      minmax(calc(${themeProps.widths.xl}px / 3 - 48px), 1fr)
    );
    grid-column-gap: ${themeProps.space.lg}px;
  }
`;

const PemerintahDaerahListGrid: React.FC = ({ children }) => {
  return <GridWrapper>{children}</GridWrapper>;
};

const PemerintahDaerahPage: NextPage<PemerintahDaerahContentPageProps> = () => {
  return (
    <PageWrapper
      title={`Daftar Situs Pemerintah Daerah | KawalCOVID19`}
      description={
        'Halaman ini berisi daftar situs resmi pemerintah daerah yang berkaitan dengan penanganan COVID-19 di Indonesia'
      }
      pageTitle="Daftar Situs Pemerintah Daerah"
    >
      <SectionHeader
        title={'Situs Pemerintah Daerah'}
        description={
          'Berikut merupakan daftar situs resmi pemerintah daerah dalam hal penanganan COVID-19'
        }
      />
      <Section>
        <Column>
          <Box mb="xxl">
            <Heading variant={700} mb="xl" as="h2">
              Provinsi
            </Heading>
            <PemerintahDaerahListGrid>
              {pemdaLinks.provinsi.map(pemda => (
                <PemerintahDaerahCard key={pemda.name} name={pemda.name} link={pemda.link} />
              ))}
            </PemerintahDaerahListGrid>
          </Box>
          <Box mb="xxl">
            <Heading variant={700} mb="xl" as="h2">
              Kabupaten/Kota
            </Heading>
            <PemerintahDaerahListGrid>
              {pemdaLinks.kabupaten_kota
                .sort((a, b) => a.name.toLowerCase().localeCompare(b.name.toLowerCase()))
                .map(pemda => (
                  <PemerintahDaerahCard
                    key={pemda.name}
                    name={pemda.name.replace(/^Kabupaten /, 'Kab. ')}
                    link={pemda.link}
                  />
                ))}
            </PemerintahDaerahListGrid>
          </Box>
        </Column>
      </Section>
    </PageWrapper>
  );
};

export default PemerintahDaerahPage;
