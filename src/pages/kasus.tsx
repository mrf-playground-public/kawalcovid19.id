import * as React from 'react';
import styled from '@emotion/styled';
import { NextPage } from 'next';
import dynamic from 'next/dynamic';
import fetchJsonP from 'fetch-jsonp';

import { PageWrapper, Content, Column } from 'components/layout';
import { Box, Stack, Text, themeProps } from 'components/design-system';
import { DetailNumbers, SecondaryNavLink } from 'modules/kasus';
import { YellowWarningIcon } from 'components/icons';
import { SectionHeader } from 'modules/posts-index';
import formatTime from 'utils/formatTime';

interface KasusProps {
  errors?: string;
}
const EmbeddedVisualizationNoSSR = dynamic(() => import('modules/kasus/EmbeddedVisualization'), {
  ssr: false,
});

const LastUpdated = dynamic(() => import('modules/kasus/LastUpdated'), {
  ssr: false,
});

const Section = styled(Content.withComponent('section'))`
  min-height: 1101px;
`;

const NavGrid = styled(Box)`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  max-width: 25%;
`;

const NavContainer = styled(Box)`
  display: grid;
  grid-template-columns: 1fr 2fr;
`;

const SummaryNumberDescWrapper = styled(Box)`
  display: grid;
  grid-template-columns: 1fr 5fr;
`;

const VisualizationGrid = styled(Box)`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 24px;

  ${themeProps.mediaQueries.lg} {
    grid-template-columns: 276px auto;
  }
`;

const updatedAt = formatTime(new Date(), 'longest');
const embedTokenUrl =
  'https://kc19-fatality-news.azurewebsites.net/api/fatalitystat?code=12R0LqAqwYeGfPFpFtHfaAzJXN9eCaDoSqIwZidnPk5TzQnt2a5UWg==';

const VisualizationByCategorySection: React.FC<VisualizationByCategoryProps> = ({
  embedToken,
  embedUrl,
  reportId,
}) => {
  const [activeVis, setActiveVis] = React.useState('provinsi');

  const onNavClick = (activeVisType: 'provinsi' | 'usia' | 'profesi') => {
    setActiveVis(activeVisType);
  };

  const vis = ['provinsi', 'usia', 'profesi'].includes(activeVis) && (
    <EmbeddedVisualizationNoSSR
      id={activeVis}
      embedToken={embedToken}
      embedUrl={embedUrl}
      reportId={reportId}
      currentPageName={activeVis}
      height="100%"
      isActive
    />
  );

  return (
    <Box margin={0}>
      <Box>
        <Box>
          <Text variant={100} color="accents07" letterSpacing="0.1em">
            LIHAT BERDASARKAN
          </Text>
        </Box>
        <Box>
          <NavContainer>
            <NavGrid>
              <SecondaryNavLink
                isActive={activeVis === 'provinsi'}
                title="Daerah"
                onClick={() => onNavClick('provinsi')}
              />
              <SecondaryNavLink
                isActive={activeVis === 'usia'}
                title="Usia"
                onClick={() => onNavClick('usia')}
              />
              <SecondaryNavLink
                isActive={activeVis === 'profesi'}
                title="Profesi"
                onClick={() => onNavClick('profesi')}
              />
            </NavGrid>
          </NavContainer>
        </Box>
      </Box>
      <br />
      <Box
        width="100%"
        height="72%"
        maxHeight={500}
        borderRadius={15}
        overflow="hidden"
        backgroundColor="card"
      >
        {vis}
      </Box>
    </Box>
  );
};

const KasusPage: NextPage<KasusProps> = () => {
  const [embedToken, setEmbedtoken] = React.useState('');
  const [embedUrl, setEmbedUrl] = React.useState('');
  const [reportId, setReportId] = React.useState('');
  const fetch = (url: string) => {
    fetchJsonP(url, {
      jsonpCallbackFunction: 'callback',
    })
      .then(r => r.json())
      .then(r => {
        setEmbedtoken(r.EmbedToken);
        setEmbedUrl(r.EmbedUrl);
        setReportId(r.ReportId);
      });
  };

  React.useEffect(() => {
    fetch(embedTokenUrl);
  }, []);

  const isKematianDiverifikasiActive = true;
  return (
    <PageWrapper pageTitle="Kasus">
      <SectionHeader
        title="Fatality News"
        description={`Kami merangkum data kematian COVID-19 dari berbagai portal berita
        dan menyajikan insightnya pada anda.`}
      />
      <Section>
        <Column>
          <Stack spacing="xl" color="accents07">
            <VisualizationGrid>
              <Box>
                <Box>
                  <Box
                    display="flex"
                    flexDirection="column"
                    backgroundColor="card"
                    borderRadius={15}
                    overflow="hidden"
                  >
                    <Box height={120} overflow="hidden">
                      <EmbeddedVisualizationNoSSR
                        id="reportContainer"
                        currentPageName="kematianDiverifikasi"
                        embedToken={embedToken}
                        embedUrl={embedUrl}
                        reportId={reportId}
                        isActive={isKematianDiverifikasiActive}
                      />
                    </Box>
                    <SummaryNumberDescWrapper py="sm">
                      <Box
                        height="100px"
                        display="flex"
                        justifyContent="center"
                        alignItems="center"
                      >
                        <Box>
                          <YellowWarningIcon width={30} height={30} />
                        </Box>
                      </Box>
                      <Box padding="5px 15px">
                        <Box>
                          <Text variant={400}>berita kematian diverifikasi</Text>
                        </Box>
                        <hr style={{ border: '0.5px solid #666B73' }} />
                        <Box>
                          <Text variant={100}>
                            Data diverifikasi oleh relawan kawalcovid19. Data ini TIDAK mewakili
                            data aktual kematian oleh COVID-19
                          </Text>
                        </Box>
                      </Box>
                    </SummaryNumberDescWrapper>
                  </Box>
                </Box>
                <br />
                <Box marginBottom={50}>
                  <Text variant={100} color="accents07" letterSpacing="0.1em">
                    RINCIAN
                  </Text>
                  <hr style={{ border: '0.5px solid #666B73' }} />
                  <DetailNumbers embedToken={embedToken} embedUrl={embedUrl} reportId={reportId} />
                </Box>
                <LastUpdated updatedAt={updatedAt} />
              </Box>
              <VisualizationByCategorySection
                embedToken={embedToken}
                embedUrl={embedUrl}
                reportId={reportId}
              />
            </VisualizationGrid>
          </Stack>
        </Column>
      </Section>
    </PageWrapper>
  );
};

interface VisualizationByCategoryProps {
  embedToken: string;
  embedUrl: string;
  reportId: string;
}

export default KasusPage;
