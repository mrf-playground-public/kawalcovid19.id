import querystring from 'query-string';
import fetch from './fetch';

export default async function wp<TResponse = unknown>(
  input: string,
  query?: { [key: string]: unknown },
  init?: RequestInit
): Promise<TResponse> {
  const path = input.replace(/^\/+/g, '');
  const qs = query ? `&${querystring.stringify(query)}` : '';
  const data = await fetch<TResponse>(
    `${process.env.NEXT_PUBLIC_WORDPRESS_API_BASE}${path}${qs}`,
    init
  );
  return data;
}
